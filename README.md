# FGU Arbitrary Arcana

Easily create spellbooks for loot and arcana-based mayhem!

## Features
* item interface to create spellbooks with property preferences
* drag and drop your creations to NPCs to easily create spellcasting stat blocks

## Credits
* fxg42 for the original project idea and basis in [Random Spellbook Generator](https://forge.fantasygrounds.com/shop/items/189/view)
* random spellbook names provided by [donjon.bin.sh](https://donjon.bin.sh/d20/random/#type=arcane_tome;arcane_tome-rank=minor)
