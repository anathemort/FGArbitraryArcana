# FGU Arbitrary Arcana
## Changelog

### 1.2.1
* fixed UI control deprecation warning
* verified compatibility with FGU v4.4.8

### 1.2.0
* Refactored spell builder for 3.5E/PF1E support
* Fixed 5E NPC spellcasting trait replacement
* Fixed PF2E Tradition matching

### 1.1.0
* Added checkbox to exclude a preference criterion

### 1.0.0
* Initial release
