sRuleset = "";
tPreferenceOptions = {};

function onInit()
    Interface.onDesktopInit = onDesktopInit;
end

function onDesktopInit()
    sRuleset = User.getRulesetName();

    self.collectDataSources();
    self.addToItemsWindow();
end

function addToItemsWindow()
    local aButtons = LibraryData.getIndexButtons("item");
    local aRecordOverrides = {};

    for k, v in ipairs(aButtons) do
        aRecordOverrides[k] = v;
    end

    table.insert(aRecordOverrides, "button_item_arbitrary_arcana");
    LibraryData.overrideRecordTypeInfo("item", { aGMListButtons = aRecordOverrides });
end

function collectDataSources()
    if sRuleset == "3.5E" or sRuleset == "PFRPG" or sRuleset == "5E" then
        local tUniqueSchools = {};
        local tUniqueSources = {};

        self.forEachSpell(function(vSpell)
            if sRuleset == "3.5E" or sRuleset == "PFRPG" then
                local sSchool = LibraryData35E.getSpellSchoolValue(vSpell);

                if sSchool ~= "" then
                    tUniqueSchools[sSchool] = true;
                end

                local aSourceLevels = LibraryData35E.getSpellSourceValue(vSpell);

                for _, vSourceLevel in pairs(aSourceLevels) do
                    local sSource = string.match(vSourceLevel, "^[^%d]+");

                    if sSource ~= nil then
                        tUniqueSources[StringManager.trim(sSource):lower()] = true;
                    end
                end
            elseif sRuleset == "5E" then
                local sSchool = DB.getValue(vSpell, "school", "");

                if sSchool ~= "" then
                    tUniqueSchools[sSchool] = true;
                end

                for _, vSource in pairs(StringManager.split(DB.getValue(vSpell, "source", ""), ",")) do
                    vSource = vSource:gsub("%([Oo]ptional%)", "");
                    vSource = vSource:gsub("%([Nn]ew%)", "");
                    vSource = StringManager.trim(vSource);

                    tUniqueSources[vSource] = true;
                end
            end
        end);

        local sSchool = Interface.getString("aa_prefopt_school");
        local sSource = Interface.getString("aa_prefopt_source");

        tPreferenceOptions[sSchool] = {};
        tPreferenceOptions[sSource] = {};

        for k in pairs(tUniqueSchools) do
            table.insert(tPreferenceOptions[sSchool], k);
        end
        table.sort(tPreferenceOptions[sSchool]);

        for k in pairs(tUniqueSources) do
            table.insert(tPreferenceOptions[sSource], k);
        end
        table.sort(tPreferenceOptions[sSource]);
    elseif sRuleset == "PFRPG2" then
        tPreferenceOptions[Interface.getString("aa_prefopt_school")] = DataCommon.spellschooltypes;
        tPreferenceOptions[Interface.getString("aa_prefopt_tradition")] = DataCommon.magictraditions;
    end

    if sRuleset == "5E" then
        tPreferenceOptions[Interface.getString("aa_prefopt_damage")] = DataCommon.dmgtypes;
    else
        tPreferenceOptions[Interface.getString("aa_prefopt_damage")] = DataCommon.energytypes;
    end
end

function createSpellbook(sName, aPreferences, aSlotValues)
    local tSpellsByLevel = self.getRandomSpellList(aPreferences, aSlotValues);
    local nodeSpellbookItem = self.createSpellbookItem(sName, tSpellsByLevel);

    Interface.openWindow("item", nodeSpellbookItem);
end

function createSpellbookItem(sName, tSpellsByLevel)
    local sSpellClass = "spelldesc";
    local nodeItem = DB.createChild("item");

    DB.setValue(nodeItem, "aa_spellbook", "number", 1);

    if sRuleset == "5E" then
        sSpellClass = "reference_spell";
    end

    local sDesc = [[
        <h>Description</h>
        <p>The spellbook contains the following spells:</p>
    ]];

    for nLevel, aSpells in ipairs(tSpellsByLevel) do
        if #aSpells > 0 then
            sDesc = sDesc .. [[
                <p><b>]] .. nLevel .. (nLevel == 1 and "st" or (nLevel == 2 and "nd" or (nLevel == 3 and "rd" or "th"))) .. [[ level spells:</b></p>
                <linklist>
            ]];

            for _, vSpell in ipairs(aSpells) do
                local nodeSpell = DB.createChild(DB.getPath(nodeItem, "aa_spells"));
                DB.setValue(nodeSpell, "record", "string", vSpell.getPath());

                sDesc = sDesc .. [[
                    <link class="]] .. sSpellClass .. [[" recordname="]] .. vSpell.getNodeName() .. [[">
                        <b>Spell:</b> ]] .. DB.getValue(vSpell, "name", "") .. [[
                    </link>
                ]];
            end

            sDesc = sDesc .. "</linklist>";
        end
    end

    sDesc = sDesc .. [[
        <p><b>Spellbook Notes</b></p>
        <p>Essential for wizards, a spellbook is a leather-bound tome with 100 blank vellum pages suitable for recording spells.</p>
    ]];

    DB.setValue(nodeItem, "locked", "number", 0);
    DB.setValue(nodeItem, "nonid_name", "string", "Spellbook");
    DB.setValue(nodeItem, "name", "string", sName);
    DB.setValue(nodeItem, "type", "string", "Wondrous Item");
    DB.setValue(nodeItem, "rarity", "string", "Common");
    DB.setValue(nodeItem, "cost", "string", "50 - 100 gp");
    DB.setValue(nodeItem, "weight", "number", 3);
    DB.setValue(nodeItem, "description", "formattedtext", sDesc);

    return nodeItem;
end

function forEachSpell(fCallback)
    local aMappings = LibraryData.getMappings("spell");

    for i = 1, #aMappings do
        local tSpellMappingChildren = DB.getChildrenGlobal(aMappings[i]);

        for j = 1, #tSpellMappingChildren do
            local vSpell = tSpellMappingChildren[j];

            if sRuleset == "3.5E" or sRuleset == "PFRPG" then
                local aSourceLevels = LibraryData35E.getSpellSourceValue(vSpell);
                local aSources = {};
                local aUniqueSources = {};
                local aLevels = {};
                local aUniqueLevels = {};

                for k = 1, #aSourceLevels do
                    local sSource = string.match(aSourceLevels[k], "^[^%d]+");
                    local sLevel = string.match(aSourceLevels[k], "(%d+)");

                    aSources[sSource:lower()] = true;
                    aLevels[sLevel] = true;
                end

                for k in pairs(aSources) do
                    table.insert(aUniqueSources, k);
                end

                for k in pairs(aLevels) do
                    table.insert(aUniqueLevels, tonumber(k));
                end

                vSpell.aSources = aUniqueSources;
                vSpell.aLevels = aUniqueLevels;
                fCallback(vSpell);
            else
                fCallback(vSpell);
            end
        end
    end
end

function getRandomSpellList(aPreferences, aSlotValues)
    local tSpellsByLevel = self.getRankedAndSortedSpellsByLevel(aPreferences, aSlotValues);
    local tSelectedSpells = {};

    for nLevel, tSpells in ipairs(tSpellsByLevel) do
        local nNumSpellsForLevel = aSlotValues[nLevel] or 0;

        if #tSpells > 0 and nNumSpellsForLevel > 0 then
            for i = 1, nNumSpellsForLevel do
                if not tSelectedSpells[nLevel] then
                    tSelectedSpells[nLevel] = {};
                end

                if i <= #tSpells then
                    table.insert(tSelectedSpells[nLevel], tSpells[i].vSpell);
                end
            end

            table.sort(tSelectedSpells[nLevel], function(a, b)
                return DB.getValue(a, "name", ""):lower() < DB.getValue(b, "name", ""):lower();
            end);
        end
    end

    return tSelectedSpells;
end

function getRankedAndSortedSpellsByLevel(aPreferences, aSlotValues)
    local tSpellsByLevel = {};

    -- get all available spells, grouped by level, with a calculated preference weight
    self.forEachSpell(function(vSpell)
        local nLevel = 0;
        local sSchool = "";
        local sSource = "";

        if sRuleset == "3.5E" or sRuleset == "PFRPG" then
            if vSpell.aLevels == nil or #vSpell.aLevels == 0 then
                nLevel = 0;
            else
                for i = 1, #vSpell.aLevels do
                    if nLevel == 0 or vSpell.aLevels[i] < nLevel then
                        nLevel = vSpell.aLevels[i];
                    end
                end
            end

            sSchool = LibraryData35E.getSpellSchoolValue(vSpell):lower();
            sSource = table.concat(vSpell.aSources, ",");
        elseif sRuleset == "PFRPG2" then
            local aSpellSchools = LibraryDataPF2.getSpellSchool(vSpell);

            if aSpellSchools[1] ~= nil then
                sSchool = aSpellSchools[1]:lower();
            end

            nLevel = DB.getValue(vSpell, "level", 0);
            sSource = DB.getValue(vSpell, "traditions", ""):lower();
        else
            nLevel = DB.getValue(vSpell, "level", 0);
            sSchool = DB.getValue(vSpell, "school", ""):lower();
            sSource = DB.getValue(vSpell, "source", ""):lower();
        end

        if nLevel == 0 or aSlotValues[nLevel] == nil or aSlotValues[nLevel] == 0 then
            return;
        end

        if not tSpellsByLevel[nLevel] then
            tSpellsByLevel[nLevel] = {};
        end

        local nRank = 0;
        local bExclude = false;

        for i = 1, #aPreferences do
            local vOption = aPreferences[i];

            if vOption.preference == "source" and sSource:find(vOption.value) then
                if vOption.exclude == 1 then
                    bExclude = true;
                    nRank = -1;
                else
                    nRank = nRank + vOption.weight;
                end
            elseif vOption.preference == "school" and sSchool:find(vOption.value) then
                if vOption.exclude == 1 then
                    bExclude = true;
                    nRank = -1;
                else
                    nRank = nRank + vOption.weight;
                end
            elseif vOption.preference == "damage type" then
                local sDesc = DB.getValue(vSpell, "description", ""):lower();
                local sName = DB.getValue(vSpell, "name", ""):lower();

                if sDesc:find(vOption.value) or sName:find(vOption.value) then

                    if vOption.exclude == 1 then
                        bExclude = true;
                        nRank = -1;
                    else
                        nRank = nRank + vOption.weight;
                    end
                end
            end
        end

        if not bExclude then
            table.insert(tSpellsByLevel[nLevel], { nRank = nRank, vSpell = vSpell });
        end
    end);

    -- shuffle all the spells, then sort by weighted rank
    for _, tSpells in ipairs(tSpellsByLevel) do
        for i = #tSpells, 2, -1 do
            local j = math.random(i);
            tSpells[i], tSpells[j] = tSpells[j], tSpells[i];
        end

        table.sort(tSpells, function(a, b)
            return a.nRank > b.nRank;
        end);
    end

    return tSpellsByLevel;
end
