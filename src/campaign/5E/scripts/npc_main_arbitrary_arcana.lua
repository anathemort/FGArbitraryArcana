function onInit()
    if super.onInit then
        super.onInit();
    end
end

function addSpellcastingTrait(tSpellsByLevel)
    self.pruneExistingSpellcastingTraits();

    local nodeNPC = getDatabaseNode();
    local nodeTrait = DB.createChild(DB.getPath(nodeNPC, "traits"));
    local sDesc = "The NPC's spellcasting ability is Intelligence. They have the following spells prepared:";

    for nLevel, tSpells in ipairs(tSpellsByLevel) do
        if nLevel == 0 then
            sDesc = sDesc .. "\rCantrips (at will): ";
        else
            if nLevel == 1 then
                sDesc = sDesc .. "\r1st";
            elseif nLevel == 2 then
                sDesc = sDesc .. "\r2nd";
            elseif nLevel == 3 then
                sDesc = sDesc .. "\r3rd";
            else
                sDesc = sDesc .. "\r" .. nLevel .. "th";
            end

            sDesc = sDesc .. " level (" .. #tSpells .. " slots): ";
        end

        for i = 1, #tSpells do
            if i > 1 then
                sDesc = sDesc .. ", ";
            end

            sDesc = sDesc .. DB.getValue(tSpells[i], "name", ""):lower();
        end
    end

    DB.setValue(nodeTrait, "name", "string", "Spellcasting (Arbitrary Arcana)");
    DB.setValue(nodeTrait, "desc", "string", sDesc);

    CampaignDataManager2.updateNPCSpellcasting(nodeNPC, nodeTrait);
end

function addSpellsFromBook(tSpellsByLevel)
    local nodeNPC = getDatabaseNode();

    DB.deleteChildren(nodeNPC, "spells");

    for _, tSpells in ipairs(tSpellsByLevel) do
        for _, nodeSpell in ipairs(tSpells) do
            CampaignDataManager2.addNPCSpell(nodeNPC, nodeSpell, false);
        end
    end
end

function convertSpellListToTable(aSpells)
    local tSpellsByLevel = {};

    for _,v in pairs(aSpells) do
        local sRecord = DB.getValue(v, "record", "");

        if sRecord ~= "" then
            local nodeSpell = DB.findNode(sRecord);

            if nodeSpell ~= nil then
                local nLevel = DB.getValue(nodeSpell, "level", 0);

                if tSpellsByLevel[nLevel] == nil then
                    tSpellsByLevel[nLevel] = {};
                end

                table.insert(tSpellsByLevel[nLevel], nodeSpell);
            end
        end
    end

    for _, tSpells in pairs(tSpellsByLevel) do
        table.sort(tSpells, function(a, b)
            return DB.getValue(a, "name", ""):lower() < DB.getValue(b, "name", ""):lower();
        end);
    end

    return tSpellsByLevel;
end

function onDrop(x, y, dragdata)
    if super.onDrop then
        super.onDrop(x, y, dragdata);
    end

    if WindowManager.getReadOnlyState(getDatabaseNode()) then
        return;
    end

    if dragdata.isType("shortcut") then
        local sClass = dragdata.getShortcutData();
        local nodeSource = dragdata.getDatabaseNode();

        if sClass == "item" and DB.getValue(nodeSource, "aa_spellbook", 0) == 1 then
            local tSpellsByLevel = self.convertSpellListToTable(DB.getChildren(nodeSource, "aa_spells"));

            if #tSpellsByLevel > 0 then
                self.addSpellcastingTrait(tSpellsByLevel);
            end
        end
    end
end

function pruneExistingSpellcastingTraits()
    local nodeNPC = getDatabaseNode();

    for _, v in pairs(DB.getChildren(nodeNPC, "traits")) do
        if DB.getValue(v, "name", ""):lower():match("^spellcasting") then
            DB.deleteNode(v);
        end
    end

    DB.deleteChild(nodeNPC, "spells");
end
