function onInit()
    if super.onInit then
        super.onInit();
    end
end

function addSpellsFromBook(tSpellsByLevel)
    local nodeSpellClass = spellclasslist.createWindow();
    DB.setValue(getDatabaseNode(), "spellmode", "string", "standard");

    for nLevel, tSpells in ipairs(tSpellsByLevel) do
        for i = 1, #tSpells do
            SpellManager.addSpell(tSpells[i], nodeSpellClass.getDatabaseNode(), nLevel);
        end
    end
end

function convertSpellListToTable(aSpells)
    local sRuleset = User.getRulesetName();
    local tSpellsByLevel = {};

    for _,v in pairs(aSpells) do
        local sRecord = DB.getValue(v, "record", "");

        if sRecord ~= "" then
            local nodeSpell = DB.findNode(sRecord);

            if nodeSpell ~= nil then
                local nLevel = 0;

                if sRuleset == "3.5E" or sRuleset == "PFRPG" then
                    local sSource = LibraryData35E.getSpellSourceValue(nodeSpell)[1];
                    local sLevelSplit = StringManager.split(sSource, " ", true);

                    nLevel = tonumber(sLevelSplit[2]);
                elseif sRuleset == "PFRPG2" then
                    nLevel = DB.getValue(nodeSpell, "level", 0);
                end

                if tSpellsByLevel[nLevel] == nil then
                    tSpellsByLevel[nLevel] = {};
                end

                table.insert(tSpellsByLevel[nLevel], nodeSpell);
            end
        end
    end

    for _, tSpells in pairs(tSpellsByLevel) do
        table.sort(tSpells, function(a, b)
            return DB.getValue(a, "name", ""):lower() < DB.getValue(b, "name", ""):lower();
        end);
    end

    return tSpellsByLevel;
end

function onDrop(x, y, dragdata)
    if super.onDrop then
        super.onDrop(x, y, dragdata);
    end

    if WindowManager.getReadOnlyState(getDatabaseNode()) then
        return;
    end

    if dragdata.isType("shortcut") then
        local sClass = dragdata.getShortcutData();
        local nodeSource = dragdata.getDatabaseNode();

        if sClass == "item" and DB.getValue(nodeSource, "aa_spellbook", 0) == 1 then
            local tSpellsByLevel = self.convertSpellListToTable(DB.getChildren(nodeSource, "aa_spells"));

            if #tSpellsByLevel > 0 then
                self.addSpellsFromBook(tSpellsByLevel);
            end
        end
    end
end
