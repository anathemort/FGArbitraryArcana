local aPreferenceOptions = { "" };
local tPreferenceValueOptions = {};

function onInit()
    for k, v in pairs(ArbitraryArcanaManager.tPreferenceOptions) do
        table.insert(aPreferenceOptions, k);
        tPreferenceValueOptions[k] = v;
    end

    preference_dropdown.addItems(aPreferenceOptions);
    preference_dropdown.onSelect = self.onPreferenceChange;
end

function delete()
    windowlist.removeEntry(self);
end

function onPreferenceChange(sValue)
    preference_value_dropdown.clear();
    preference_value_dropdown.setValue("");

    if tPreferenceValueOptions[sValue] ~= nil then
        preference_value_dropdown.addItems(tPreferenceValueOptions[sValue]);
        preference_value_dropdown.setListIndex(1);
    end
end
