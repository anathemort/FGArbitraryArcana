function onInit()
    local sRuleset = User.getRulesetName();

    if sRuleset == "PFRPG2" then
        slotnumber10.setVisible(true);
        slotlabel10.setVisible(true);
    end
end

function getSelectedPreferences()
    local sRuleset = User.getRulesetName();
    local aResults = {};

    for _, w in ipairs(preferenceslist.getWindows()) do
        local sPreference = w.preference_dropdown.getSelectedValue();
        local sPreferenceValue = w.preference_value_dropdown.getSelectedValue();

        if sRuleset == "PFRPG2" and sPreference:lower() == "tradition" then
            sPreference = "source";
        end

        if sPreference ~= nil and sPreferenceValue ~= nil then
            table.insert(aResults, {
                preference = sPreference:lower(),
                value = sPreferenceValue:lower(),
                weight = w.preference_weight.getValue(),
                exclude = w.exclude_toggle.getValue()
            });
        end
    end

    return aResults;
end

function getSelectedSlotValues()
    local aSlotControls = self.getSlotControls();
    local aResults = {};

    for i, ctrl in ipairs(aSlotControls) do
        aResults[i] = ctrl.getValue();
    end

    return aResults;
end

function getSlotControls()
    local aChildren = getControls();
    local aControls = {};

    for _, ctrl in pairs(aChildren) do
        local _, _, sSlotNumber = ctrl.getName():find("slotnumber(%d+)");

        if sSlotNumber then
            aControls[tonumber(sSlotNumber)] = ctrl;
        end
    end

    return aControls;
end

function onGenerateClick()
    local aPrefs = self.getSelectedPreferences();
    local aSlotValues = self.getSelectedSlotValues();

    ArbitraryArcanaManager.createSpellbook(name.getValue(), aPrefs, aSlotValues);
end

function onRollNameClick()
    Interface.openURL("https://donjon.bin.sh/d20/random/rpc.cgi?type=Arcane+Tome&rank=Minor&n=1", function(_, response)
        if response ~= nil then
            local _, _, sName = response:find('^%[%"([^:]+):');
            name.setValue(sName);
        else
            name.setValue("");
        end
    end);
end

function onRollSlotsClick()
    -- Create a parabloic distribution of a random amount of total slots across slot levels, weighted to favor lower levels
    local nMaxTotalSlots = math.random(8, 16);
    local nMaxSlot = math.random(5, (slotnumber10.isVisible() and 10) or 9);
    local nMinPerSlot = 0;
    local nMid = (nMaxTotalSlots - nMinPerSlot) / 2;
    local aResults = {};

    for _ = nMaxSlot, 1, -1 do
        local nRand = math.floor(nMid * math.pow(math.random(), 0.5));

        if math.random() < 0.5 then
            nRand = nMid - nRand;
        else
            nRand = nMid + nRand;
        end

        table.insert(aResults, math.floor(nRand));
    end

    table.sort(aResults, function(a, b)
        return a > b;
    end);

    local aSlotControls = self.getSlotControls();

    for i, ctrl in ipairs(aSlotControls) do
        ctrl.setValue(tonumber(aResults[i]));
    end
end
